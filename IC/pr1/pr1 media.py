## Calcular la media aritmética

m = 0
num = input("Introduce el primer número: ")
try:
    num = float(num)
    n = 1
    while num != "":
        m = m + num
        num = input("Introduce el siguiente número: ")
        try:
            num = float(num)
            n = n + 1
        except ValueError:
            break
    print("La media es: ", m/n)
except ValueError:
    print("Error")
