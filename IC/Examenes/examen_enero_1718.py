#EX1
def rot13(cadena):
    newStr = str()
    for words in cadena:
        for letters in words:
            if letters not in alfa:
                newStr = newStr + letters
            else:
                newStr = newStr + alfa[(alfa.find(letters) + 13)%len(alfa)]
    return newStr

#EX2
def factorizacion(num):
    factores = list()
    for i in range(2,num):
        while num%i == 0:
            factores.append(i)
            num = num / i
    if len(factores) == 0:
        factores.append(num)
    return factores

def factDict(num):
    factores = dict()
    for i in range(2,int(num/2)):
        mult = 0
        while num % i == 0:
            num = num / i
            mult = mult + 1
            factores[i] = mult
    return factores

def multiDict(num):
    factoresFinal = dict()
    factores = list()
    for i in range(2,num):
        while num%i == 0:
            factores.append(i)
            num = num / i
    if len(factores) == 0:
        factores.append(num)
    for j in range(0,factores[len(factores)-1]+1):
        m = factores.count(j)
        if m > 0:
            if m not in factoresFinal:
                factoresFinal[factores.count(j)] = [j]
            else:
                mList = factoresFinal[m]
                mList = mList + [j]
                factoresFinal[m] = mList
            
    return factoresFinal

#EX3
    
def gradeReader(archivo):
    with open(archivo) as f:
        doc = f.readlines()
        n = int(doc[0])
        m = int(doc[1])
        names = list()
        subj = list()
        nMatrix = list()
        for filas in range(0,n):
            nMatrix.append(list())
            for columnas in range(0,m):
                nMatrix[filas].append(list())
        for lines in doc[2:len(doc)]:
            lines = list(lines.split())
            if lines[0] not in names:
                names.append(lines[0])
            if lines[1] not in subj:
                subj.append(lines[1])
            nMatrix[names.index(lines[0])][subj.index(lines[1])] = float(lines[2])
        return names, subj, nMatrix

def gradeWriter(names, subjs, nMatrix, archivo):
    f = open(archivo, mode = "w")
    for name in names:
        f.write(name + " ")
        grade = float()
        for subj in subjs:
            f.write(subj + " ")
            f.write(str(nMatrix[names.index(name)][subjs.index(subj)])+ " ")
            print(nMatrix[names.index(name)][subjs.index(subj)])
            grade = grade + float(nMatrix[names.index(name)][subjs.index(subj)])
        f.write(str(grade/len(subjs)) + "\n")
    f.close()

a,b,c = gradeReader("test.txt")
print(gradeWriter(a,b,c,"test2.txt"))
            
