#EX1
def bienParentizado(archivo):
    with open(archivo) as f:
        doc = f.readlines()
        for strings in doc:
            openPar = 0
            closePar = 0
            for chars in strings:
                if chars == "(":
                    openPar = openPar + 1
                if chars == ")":
                    closePar = closePar + 1
            if openPar != closePar:
                return "Mal parentizado"
        return "Bien parentizado"
#print(bienParentizado("testparentesis.txt"))


#EX2
def checkGemelos(num1, num2):
    if num1 == 2 and num2 == 3:
        return 1
    elif num2 == num1 + 2:
        for i in range(2,int(num1/2)):
            if num1%i == 0:
                return 0
        for i in range(2,int(num2/2)):
            if num2%i == 0:
                return 0
        return 1
    else:
        return 0

pareja = str()
listaGemelos = list()
while True:
    #pareja = input("Introduce un par de numeros o 000: ")
    pareja = "000"
    if pareja == "000":
        break
    a = int(pareja.split()[0]), int(pareja.split()[1])
    if checkGemelos(a[0],a[1]) == 1:
        if listaGemelos == list():
            listaGemelos.append(a)
        elif a not in listaGemelos:
            p = 0
            for pairs in listaGemelos:
                if int(pairs[1]) > int(a[0]):
                    listaGemelos.insert(p,a)
                    break
                p = p + 1

#print("Las parejas de numeros primos gemelos son: ", listaGemelos)

#EX3

def uneD(ls1, ls2):
    ls3 = list()
    for elements in ls2:
        if elements not in ls1:
            ls1.append(elements)
    a = len(ls1)
    while len(ls3) < a:
        add = ls1[0]
        for elements in ls1:
            if add < elements:
                add = elements
        ls1.remove(add)
        ls3.append(add)        
    return ls3

def listRotate(ls, n):
    lsN = [0 for elements in ls]
    i = 0
    for elements in ls:
        lsN[(i+n-1)%len(lsN)] = elements
        i = i + 1
    return lsN

def checkMatrix(ls, matrix):
    if len(ls) != len(matrix) and len(ls) != len(matrix[0]):
        return False
    elif len(ls) == len(matrix):
        for filas in matrix:
            if filas == ls:
                return True
        for j in range(len(matrix[0])):
            col = list()
            for i in range(len(matrix)):
                col.append(matrix[i][j])
            if col == ls:
                return True

#EX4
def countUsers(lista):
    userCount = dict()
    for tuits in lista:
        for words in tuits.split():
            if words[0] == "@":
                if words not in userCount:
                    userCount[words] = 1
                else:
                    userCount[words] = userCount[words] + 1
    return userCount

def reverseDict(users):
    freqs = dict()
    for user in users:
        print(user)
        add = users.get(user,0)
        if add not in freqs:
            freqs[add] = [user]
        else:
            freqs[add] = freqs[add] + [user]
    return freqs

def getMax(freqs):
    add = int()
    for freq in freqs:
        if freq > add:
            add = freq
    return freqs[add]
tuits= ["El día termina como siempre, escuchando un tema de @cantante","es verdad, no tiene precio @cantante y @tu", "muchas gracias @tu", "@yo y @cantante despidiendonos como siempre", "por lo que parece has visto el documental @yo y @tu"]
print(getMax(reverseDict(countUsers(tuits))))







#print(checkMatrix([1,2,1],[[1,2,3],[2,1,3],[1,3,2]]))
#print(listRotate([3,5,8,23,65],3))
#print(uneD([3,5,8,23,65],[8,10,23,65,67,80]))
























                
