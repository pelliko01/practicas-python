#EX1
def checkRedondo(num):
    div = list()
    for divisores in range(1, num + 1):
        if num%divisores == 0:
            div.append(divisores)
    if len(div) in div:
        return True
    else:
        return False

def xNumbers(x):
    if type(x) != int:
        x = 10
    for nums in range(1,x+1):
        print(nums,":",checkRedondo(nums))



#EX2
def ordenarMayor(listA):
    listB = list()
    a = int()
    i = 0
    l = len(listA)
    while i <= len(listA):
        if listA == list():
            break
        else:
            listB.append(max(listA))
            for i in range(0,listA.count(max(listA))):
                listA.remove(max(listA))
        print(listB)
            
def desplazarLista(listA,n):
    i = 0
    if n < 0:
        n = len(listA) + n
    listA = listA * 2
    listB = listA[int(len(listA)/2 - n) : int(len(listA)-n)]
    print(listB)

#lista = [9,2,25,6,8,9]
#ordenarMayor(lista)
#desplazarLista(lista,3)

def checkPertenece(matrix, ls):
    if len(ls) != len(matrix) and len(ls) != len(matrix[0]):
        return False
    else:
        for filas in matrix:
            if filas == ls:
                return True 
        for j in range(len(matrix[0])):
            check = list()
            for i in range(len(matrix)):
                check.append(matrix[i][j])
            if check == ls:
                return True
        check = list()
        checkT = list()
        for j in range(len(matrix[0])):
            check.append(matrix[j][j])
            o = len(matrix[0])-1-j
            checkT.append(matrix[j][o])
        if check == ls:
            return True,"diagonal principal"
        if checkT == ls:
            return True, "diagonal secundaria"

def dniTrue(dni):
    if len(dni) != 9:
        return False
    if dni[8] not in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        return False
    for elements in dni[0:8]:
        if elements not in "1234567890":
            return False
    return True

def dniGood(dni):
    if dniTrue(dni) == True:
        keys = {"0" : "T", "1":"R", "2":"W", "3":"A", "4":"G", "5": "M", "6":"Y", "7":"F", "8":"P", "9":"D", "10":"X", "11":"B", "12":"N", "13":"J", "14":"Z", "15":"S", "16":"Q", "17":"V", "18":"H", "19":"L", "20": "C", "21":"K", "22":"E"}
        num = int(dni[0:8])%23
        if keys[str(num)] == dni[8]:
            return True
        else:
            return False
    else:
        return False
def readDni(file):
    f = open(file, mode = "r")
    dnis = f.read().split()
    dicDNI = dict()
    print(dnis)
    for elements in dnis:
        if dniGood(elements) == True:
            if elements[8] in dicDNI:
                dicDNI[elements[8]] = dicDNI[elements[8]] + [elements]
            else:
                dicDNI[elements[8]] = [elements]
    f.close()
    return dicDNI






    
print(readDni("dnis.txt"))




















a = True
listaDef = list()
while a == True:
    suLista = input("Introduce secuencias de numeros o #: ")
    if suLista == "#":
        break
    suLista = list(suLista.split())
    for nums in suLista:
        if nums not in listaDef:
            listaDef.append(nums)
listaDef.sort()
redondos = list()
for elements in listaDef:
    if checkRedondo(int(elements)) == True:
        redondos.append(elements)
losNumeros = str()
for red in redondos:
    losNumeros = losNumeros + " " + str(red)
print("Los numeros redondos introducidos son:", losNumeros)
