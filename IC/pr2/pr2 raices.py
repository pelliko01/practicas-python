        #   Vamos a hacer un programa que calcule las soluciones de una ecuación de segundo grado   #

def operar(x, y, z):
    if x == 0:
        if y == 0:
            if z == 0:
                return "0 = 0"
            else:
                return "Resultado contradictorio"
        elif z == 0:
            return "Solución nula"
        else:
            sol  = -z/y
            return sol
    else:
        if (y**2 - 4*x*z) < 0:
            sol_c = sqrt(-(y**2 - 4*x*z))/2*x
            print(int(-y/2*x),"+",sol_c,"* i","y",int(-y/2*x),"-",sol_c,"* i")
            return "Compleja"
        else:
            sol_mas = (-y + sqrt(y**2 - 4*x*z))/(2*x)
            sol_menos = (-y - sqrt(y**2 - 4*x*z))/(2*x)

            return sol_mas, sol_menos


from math import sqrt     
sol_c = 0
sol_mas = 0
sol_menos = 0

a = float(input("Introduzca el coeficiente cuadrático: "))
b = float(input("Introduzca el coeficiente lineal: "))
c = float(input("Introduzca el coeficiente independiente: "))

print("La solución de la ecuación es: ",operar(a,b,c))
