        #   Cifrado de vigenère   #
        #separar en funciones
        ##comprobar lo de los caracteres que pasan sin cifrar por no estar en el diccionario
def chooseCifrado():
    cifradoN = int(input("Pulse 1 para cifrar una frase, o pulse 2 para descifrarla: "))
    if cifradoN == 1:
        return True
    elif cifradoN == 2:
        return False
    else:
        print("Debe escoger entre cifrado y descifrado")

def pos(x,y):
    return y.index(x)
#ABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890
alfa = "abcdefghijklmnopqrstuvwxyz "
result = ""
i = 0
if (chooseCifrado()):
    target = str(input("Introduzca la frase a traducir: "))
    key = str(input("Introduzca la clave: "))
    length = len(target)
    charPosMax = [z for z in range(0,length)]
    newKey = key * (length//len(key)) #elaboramos la clave de longitud length
    for char in key:
        if pos(char,key)<(length%len(key)):
            newKey = newKey + char
    print("Números mágicos: ")
    charPos = [pos(char,alfa) for char in target] #lista de posiciones del target
    print(charPos)
    
    charPosDef = [pos(char,alfa) for char in newKey]
    print(charPosDef)
    while i < length:
        charPosMax[i] = (charPos[i] + charPosDef[i])%len(alfa)
        i = i + 1
    print(charPosMax)
    for i in range(0,length):
        result = result + alfa[charPosMax[i]]
    print("El mensaje cifrado es: ")
    print(result)
else:
    target = str(input("Introduzca el mensaje cifrado: "))
    key = str(input("Introduzca la clave secreta: "))
    charPosMax = [z for z in range(0,len(target))]
    length = len(target)
    newKey = key * (length//len(key))
    for char in key:
        if pos(char,key)<(length%len(key)):
            newKey = newKey + char
    print("Números mágicos: ")
    charPos = [pos(char,alfa) for char in target]
    print(charPos)
    
    charPosDef = [pos(char,alfa) for char in newKey]
    print(charPosDef)
    while i < length:
        charPosMax[i] = (charPos[i] - charPosDef[i])%len(alfa)
        i = i + 1
    print(charPosMax)
    for i in range(0,length):
        result = result + alfa[charPosMax[i]]
    print("El mensaje original es: ")
    print(result)
