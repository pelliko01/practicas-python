def histograma(fichero):
    with open(fichero) as f:
        doc = f.readlines()
        dictionary = dict()
        for lines in doc:
            for words in lines.split():
                if words not in dictionary:
                    dictionary[words] = 1
                else:
                    dictionary[words] = dictionary[words] + 1
    return dictionary

def analizarHistograma(diccionario, numero):
    lista = diccionario.keys()
    listadef = []
    for clave in lista:
        if diccionario.get(clave) == numero:
            listadef = listadef + [clave]
    return listadef

def invertirHistograma(diccionario, numero):
    newdict = {}
    listaVal = diccionario.values()
    listaCla = diccionario.keys()
    for valor in listaVal:
        if valor == numero:
            newdict = { numero : analizarHistograma(histograma("test_dict.txt"),numero)}
    return newdict

def alumnosMatriculados(fichero):
    f = open(fichero)
    doc = f.readlines()
    result = dict()
    for lines in doc:
        wordPos = int()
        name = list()
        subjects = list()
        for words in lines.split():
            wordPos = wordPos + 1
            if wordPos <= 2:
                name.append(words)
            else:
                subjects.append(words)
        result[str(name)] = subjects
    f.close()
    return result.items()
    
print(histograma("test_dict.txt"))
print(analizarHistograma(histograma("test_dict.txt"),2))
print(invertirHistograma(histograma("test_dict.txt"),2))
print(alumnosMatriculados("test_dict.txt"))
