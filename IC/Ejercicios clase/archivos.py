def contarPalabra(fichero, palabra):
    f = open(fichero)
    doc = f.read()
    print(doc)
    n = 0
    for words in doc.split():
        if words == palabra:
            n = n + 1
    f.close()
    return n

def contarE(fichero):
    with open(fichero) as f:
        n = 0
        palabras = f.read().split()
        nTot = len(palabras)
        for palabra in palabras:
            for letras in palabra:
                if letras == "e":
                    n = n + 1
                    break
    return 100 * (n/nTot)

def longerLine(fichero):
    f = open(fichero)
    n = 0
    nMax = 0
    linea = ""
    lineaLong = ""
    doc = f.readlines()
    for linea in doc:
        n = len(linea)
        if n > nMax:
            nMax = n
            if linea == doc[len(doc) - 1]:
                lineaLong = linea
            else:
                lineaLong = linea[0:len(linea)-1]
    return nMax, lineaLong
    f.close()

def longerLineDict(fichero):
    with open(fichero) as f:
        n = 0
        nMax = 0
        line = str()
        longitudes = dict()
        doc = f.readlines()
        for lines in doc:
            n = len(lines)
            if n > nMax:
                nMax = n
            if lines == doc[len(doc)-1]:
                longitudes[n] = lines[0:len(lines)]
            else:
                longitudes[n] = lines[0:len(lines)-1]
        return longitudes[nMax]

def eliminarEspacios(fichero,resultado):
    f = open(fichero)
    doc = f.read().split()
    finalString = str()
    for words in doc:
        finalString = finalString + words
    f.close()
    r = open(resultado, mode = "w")
    r.write(finalString)
    r.close()
    r = open(resultado, mode = "r")
    print(r.read())
    r.close() 

def ficherosPalabra(fichero, palabra):
    with open(fichero) as f:
        lineas = f.readlines()
        t = open("ficheroCon.txt", mode = "w")
        t.close()
        r = open("ficheroSin.txt", mode = "w")
        r.close()
        for linea in lineas:
            if palabra in str(linea):
                with open("ficheroCon.txt", mode = "a") as t:
                    t.write(str(linea))
            else:
                with open("ficheroSin.txt", mode = "a") as r:
                    r.write(str(linea))
    with open("ficheroCon.txt", mode = "r") as t:
        print(t.read())
    with open("ficheroSin.txt", mode = "r") as r:
        print(r.read())

def ficherosNumeros(fichero):
    f = open(fichero)
    lineas = f.read().split("\n")
    print(lineas)
    
    for strings in lineas:
        minimo = int()
        maximo = int()
        lista = list()
        for pos in strings.split():
            pos = int(pos)
            lista.append(pos)
        print("el máximo es: ",max(lista))
        print("el mínimo es: ", min(lista))
    f.close()

ficherosNumeros("numbers.txt")
#print(ficherosPalabra("test.txt","matematicas"))
#print(longerLineDict("test.txt"))
#print(contarPalabra("test.txt", "matematicas"))
#print(contarE("test.txt"))
#eliminarEspacios("test.txt", "resultado.txt")

