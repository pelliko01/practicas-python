def pares(n):
    n = int(input("Introduzca un número: "))
    pares = []
    i = 0
    while i < n:
        if i%2 == 0:
            pares = pares + [i]
        i = i + 1
    return pares

def cribaMayus(cadena):
    resultMa = []
    resultMi = []
    mayus = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
    for words in cadena.split(" "):
        resultMi.append(words)
        for letters in range(0, len(mayus)):
            if words[0] == mayus[letters]:
                resultMa.append(words)
    print(resultMa)
    for j in range(0, len(resultMa)):
        for i in range(0, len(resultMi)):
            if resultMi[i - 1] == resultMa[j - 1]:
                resultMi.remove(resultMa[j - 1])
    print(resultMi)

def cribaPares(lista):
    result = []
    for i in range(0, len(lista)):
        if lista[i]%2 != 0:
            result.append(lista[i])
        else:
            result.append(str(lista[i]))
    return result

def cribaDobles(lista):
    listaNu = list()
    listaNu.append(lista[0])
    for number in lista:
        for numberNu in listaNu:
            if number in listaNu:
                None
            else:
                listaNu.append(number)
    return listaNu

def busquedaLista(lista,elemento):
    for i in range(0,len(lista)):
        if elemento == lista[i]:
            return "Elemento encontrado"
            break
        else:
            a = -1
    return a

def mezclaCadenas(lista1, lista2):
    for enteros in lista2:
        lista1.append(enteros)
    lista1.sort()
    print("La lista con duplicados es: ", lista1)
    print("La lista sin duplicados es: ", cribaDobles(lista1))

#def ordenacionBurbuja():

def checkSimetrica(matriz):
    if len(matriz) != len(matriz[0]):
        return False
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            if matriz[i][j] != matriz[j][i]:
                return False
    return "Es simétrica"

def sumaResta(mat1, mat2):
    #asumiendo que sean cuadradas y del mismo orden
    matRes = list()
    sumaResta = input('Escriba "suma" o "resta" para sumar o restar: ')
    for i in range(len(mat1)):
        zerolist = list()
        for i in range(len(mat1)):
            zerolist.append(0)    
        matRes.append(zerolist)
    if sumaResta == "suma":
        for i in range(len(mat1)):
            for j in range(len(mat2)):
                matRes[i][j] = mat1[i][j] + mat2[i][j]
    elif sumaResta == "resta":
        for i in range(len(mat1)):
            for j in range(len(mat2)):
                matRes[i][j] = mat1[i][j] - mat2[i][j]
    else:
        print("Comando no válido")
    return matRes



#print(cribaDobles([0,1,1,1,3,4,0,0,1,0,9,6,5,0,1,3,4,5,9,2]))
#print(busquedaLista([0,1,1,1,3,4,0,0,1,0,9,6,5,0,1,3,4,5,9,2], 9))
#print(checkSimetrica([[1,0,5],[0,1,5],[5,5,1]]))
print(sumaResta([[1,0],[0,1]],[[1,0],[0,1]]))
#print([i**3 for i in range(100)])
