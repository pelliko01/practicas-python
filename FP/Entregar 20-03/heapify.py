def heapify(h): # n = len(h)-1 (la posición 0 no se usa)
    for k in range(2,len(h)):
        j=k
        i=k//2
        item=h[k]
        while i>0 and h[i]<item:
            h[j]=h[i]
            j=i
            i=i//2
        h[j]=item

# Coste temporal
#
# t(n) = Sum{k=2-->n} (1 + Sum{l=1-->s(k)} (1) + 1)
#      = 2*(n-1) + Sum{k=2-->n} s(k)
#
# s(k): número de iteraciones del while, comprendido entre 0
#       y floor(log2(k//2)) + 1 <= log2(k)
#
# Aclaración: floor(x) = entero menor que x más grande
#             por ejemplo, floor(3.8) = 3
#
# En general, si v_ini es el valor de i en la primera iteración
# y v_fin es el valor de i en la última iteración (con v_ini>=v_fin),
# y en cada iteración i se divide por f, el número de iteraciones será:
#
# n_ite = floor(log_f(v_ini/v_fin)) + 1
#
# Por ejemplo, si v_ini=1000, v_fin=10 y f=3, será:
#
# n_ite = floor(log_3(100))+1 = floor(4.19)+1 = 5
#
# Intuitivamente, log_3(100) nos está diciendo cuántas veces es necesario
# dividir por 3 para que el efecto sea equivalente a dividir por 100
# (que es el factor que separa a los valores inicial y final)
#
# Mejor caso: s(k) = 0 para todo k
# tm(n) = 2*n - 2
#
# Peor caso: s(k) = log2(k) para todo k
# tp(n) = 2*n - 2 + Sum{k=2-->n} log2(k)
#      <= 2*n - 2 + Sum{k=2-->n} log2(n)    <-- ya que log2(k) <= log2(n)
#       = 2*n - 2 + (n-1)*log2(n)
#       = n*log2(n) + 2*n - log2(n) - 2
#
# Así pues:
# t(n) está en Omega(n)
# t(n) está en O(n*log2(n))
