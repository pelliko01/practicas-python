import time
import random

#####################
# VERSION ITERATIVA #
#####################

def comb_suma(v,s): # n = len(v)
    # sol: lista de combinaciones solución
    sol=[]
    # si s=0, añadimos la solución trivial []
    if s==0:
        sol.append([])
    l_idx=[[]] # lista de combinaciones ya exploradas
    for i in range(len(v)):
        # construimos la lista de combinaciones para la iteración i
        # añadiendo el índice i a las que teníamos en la iteración i-1
        new_idx=[]
        for idx in l_idx:
            new_idx.append(idx[:]+[i])
        # Para cada conjunto de índices, comprobamos si los elementos suman s
        for idx in new_idx:
            suma=0
            for k in idx:
                suma+=v[k]
            if suma==s:
                sol.append(idx)
        # por último, extendemos la lista de combinaciones ya exploradas
        l_idx.extend(new_idx)
    return sol

# Coste temporal
#
# Número de nuevas combinaciones en cada iteración
#
# i=0 -->  1 (total de combinaciones:  2)
# i=1 -->  2 (total de combinaciones:  4)
# i=2 -->  4 (total de combinaciones:  8)
# i=3 -->  8 (total de combinaciones: 16)
# i=4 --> 16 (total de combinaciones: 32)
#   ...
# En general, habrá 2^i nuevas combinaciones en la iteración i,
# que se obtienen añadiendo un nuevo índice (i) a las 2^i combinaciones
# existentes hasta la iteración i-1 (esto significa que el número
# de combinaciones total se duplica en cada iteración)
#
# Número de términos a sumar para comprobar las nuevas combinaciones
# en cada iteración i; lo expresamos de manera recursiva:
#
# CASO BASE:    s(0) = 1
# CASO GENERAL: s(i) = Suma{j=0-->i-1} s(j) + 2^i
#
# s(i) - s(i-1) = s(i-1) + 2^(i-1)
# s(i) = 2*s(i-1) + 2^(i-1)
#
# s(i) =                                     2*s(i-1) +   2^(i-1)
#      = 2*(2*s(i-2)+2^(i-2)) +   2^(i-1) =  4*s(i-2) + 2*2^(i-1)
#      = 4*(2*s(i-3)+2^(i-3)) + 2*2^(i-1) =  8*s(i-3) + 3*2^(i-1)
#      = 8*(2*s(i-4)+2^(i-4)) + 3*2^(i-1) = 16*s(i-4) + 4*2^(i-1)
#         ...
# En el caso general:                     = 2^k * s(i-k) + k * 2^(i-1)
#
# Para llegar al caso base, hacemos k=i y nos queda:
#
# s(i) = 2^i + i*2^(i-1)
#
# Es decir:
#
# s(0) = 2^0 + 0*2^(-1) = 1
# s(1) = 2^1 + 1*2^0 = 3
# s(2) = 2^2 + 2*2^1 = 8
# s(3) = 2^3 + 3*2^2 = 20
# s(4) = 2^4 + 4*2^3 = 48
#     ...
#
# t(n)  = 1 + Sum{i=0-->n-1} (1 + 3 * 2^i + 3 * (2^i + i*2^(i-1))) + 1
#       = 2 + Sum{i=0-->n-1} (1 + 6*2^i + 1.5*i*2^i)
#       = 2 + n + 6*(2^n - 1) + 1.5*((n-1)*2^n - 2^n + 2)
#       = 1.5*n*2^n + 3*2^n + n - 1
#
# Así pues, t(n) está en Theta(n*2^n)

# OTRA FORMA DE VERLO: la instrucción que más veces se repite,
# la que determina el número de pasos, es suma+=v[k], es decir,
# el número de pasos será proporcional al número de elementos
# que lleguemos a considerar para comprobar las sumas.
#
# El número de subconjuntos distintos de índices es:
#
# Sum{k=0-->n} (n sup k) = 2^n
#
# El número T de términos que intervienen en las sumas es:
#
# T = Sum{k=0-->n} (n sup k) * k
#
# Teniendo en cuenta que (n sup k) = (n sup n-k), podemos escribir:
#
# T = Sum{k=0-->n} (n sup k) * (n-k)
#
# Sumando las dos expresiones anteriores y diviendo por 2, tenemos:
#
# T = 0.5 * Sum{k=0-->n} (n sup k) * (k+n-k) = 0.5 * n * 2^n
#
# De aquí deducimos que t(n) está en Theta(n*2^n)

#####################
# VERSION RECURSIVA #
#####################

# l_idx: conjunto de índices
# s_parcial: suma de los elementos incluidos en l_idx
# i: número de índices considerados para construir l_idx
# v: vector de enteros
# s: suma objetivo
# La llamada original será comb_suma_rec([],0,0,v,s)
def comb_suma_rec(l_idx,s_parcial,i,v,s):
    # si ya se han considerado todos los índices en la construcción de l_idx,
    # se comprueba si l_idx es solución o no
    if i == len(v):
        if s_parcial == s:
            return [l_idx]
        else:
            return []
    # si no, se construyen soluciones a partir de l_idx
    # llamando a la propia función con l_idx extendido en una posición más
    else:
        # sol_1: soluciones NO AÑADIENDO el índice i a los índices de l_idx
        sol_1 = comb_suma_rec(l_idx[:],s_parcial,i+1,v,s)
        # sol_2: soluciones AÑADIENDO el índice i a los índices de l_idx
        sol_2 = comb_suma_rec(l_idx[:]+[i],s_parcial+v[i],i+1,v,s)
        return sol_1 + sol_2 # retornamos la unión de ambos conjuntos

# Coste temporal
#
# n = len(v)
# m = len(v)-i --> número de índices que queda por considerar
#
# t(m) = 1 + 2 * t(m-1)     para m>0
# t(0) = 1
#
# t(m) = 1 + 2*t(m-1) =  1 + 2*(1 + 2*t(m-2)) =
#      = 3 + 4*t(m-2) =  3 + 4*(1 + 2*t(m-3)) =
#      = 7 + 8*t(m-3) =  ...
#      = 2^k - 1 + 2^k * t(m-k)
#
# Haciendo k=m (para llegar al caso base), nos queda:
#
# t(m) = 2^m - 1 + 2^m * t(0)
#      = 2*2^m - 1
#
# Puesto que en la llamada principal es m=n, el coste de esa llamada será:
#
# t(n) = 2*2^n - 1
#
# Así pues, t(n) está en Theta(2^n)

######################
# PROGRAMA PRINCIPAL #
######################

def t_ite(n):
    return 1.5*n*2**n + 3*2**n + n - 1

def t_rec(n):
    return 2*2**n - 1

l=[3,12,-1,34,7,-5]
s=4
print(l)
print("Suma buscada: ",s)
print("\nSolución (iterativa):",comb_suma(l,s))
print("Solución (recursiva):",comb_suma_rec([],0,0,l,s))

print("\nTiempos versión iterativa\n")
for n in range(4,28,4):
    l=[random.randint(-10,10) for i in range(n)]
    s=random.randint(-10,10)
    t1=time.time()
    comb_suma(l,s)
    t2=time.time()
    t=t_ite(n)
    print("{0:<6d}: {1:16.6f} {2:16.6e} {3:16.6e}".format(n,t2-t1,t,(t2-t1)/t))
    
print("\nTiempos versión recursiva\n")
for n in range(4,28,4):
    l=[random.randint(-10,10) for i in range(n)]
    s=random.randint(-10,10)
    t1=time.time()
    comb_suma_rec([],0,0,l,s)
    t2=time.time()
    t=t_rec(n)
    print("{0:<6d}: {1:16.6f} {2:16.6e} {3:16.6e}".format(n,t2-t1,t,(t2-t1)/t))

############
#  SALIDA  #
############

# [3, 12, -1, 34, 7, -5]
# Suma buscada:  4
# 
# Solución (iterativa): [[0, 2, 4, 5]]
# Solución (recursiva): [[0, 2, 4, 5]]
# 
# Tiempos versión iterativa
# 
# 4     :         0.000025     1.470000e+02     1.719209e-07
# 8     :         0.000389     3.847000e+03     1.010815e-07
# 12    :         0.006480     8.602700e+04     7.532494e-08
# 16    :         0.129821     1.769487e+06     7.336636e-08
# 20    :         2.825807     3.460303e+07     8.166358e-08
# 24    :        53.930395     6.543114e+08     8.242313e-08
# 
# Tiempos versión recursiva
# 
# 4     :         0.000026     3.100000e+01     8.383105e-07
# 8     :         0.000355     5.110000e+02     6.947265e-07
# 12    :         0.005905     8.191000e+03     7.209317e-07
# 16    :         0.092269     1.310710e+05     7.039634e-07
# 20    :         1.491996     2.097151e+06     7.114396e-07
# 24    :        24.745186     3.355443e+07     7.374640e-07
