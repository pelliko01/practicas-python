def find(s2,s1):  # n = len(s1), m = len(s2)
    for i in range(len(s1)-len(s2)+1):
        j=0
        while j<len(s2) and s2[j]==s1[i+j]:
            j=j+1
        if j==len(s2):
            return i
    return None

# Coste temporal (típicamente, será m << n)
#
# Se supone que len(s) y s[i] tienen coste fijo
# Se supone también que m <= n (si m>n, el coste es fijo)
#
# t(n) = suma_{i=0-->k} (1 + suma_{j=1-->l(i)} (1) + 1) + 1
#      = 2*k + 3 + suma_{i=0-->k} (l(i))
#
# donde:
#        k está en [0,n-m] (índice de la última iteración del for)
#        l(i) está en [1,m] (número de iteraciones del while)
#
# Mejor caso: la cadena s2 se encuentra en la posición i=0 de s1;
#             esto implica que k=0 y l(0)=m
# tm(n) = m + 3
#
# Es decir, t(n) está en Omega(m)
#
# Peor caso: la cadena s2 no se encuentra en s1 --> k = n-m
#            y además se hace el máximo número de comparaciones
#            en cada iteración del for --> l(i) = m para todo i
#            por ejemplo: s1 = 'aaaaaaaaaaaaaaaaaaaaaaa', s2=aab
#            
# tp(n) = 2*(n-m) + 3 + suma_{i=0-->n-m} (m)
#       = 2*(n-m) + 3 + (n-m+1)*m
#       = n*m - m*m + 2*n - m + 3
#
# Es decir, t(n) está en O(n*m)

