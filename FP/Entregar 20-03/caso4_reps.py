def reps_lista(v):  # n = len(v)
    l = []
    for i in range(len(v)):
        j=0
        while j<len(l) and l[j][0]!=v[i]:
            j+=1
        if j==len(l):
            c=1
            for k in range(i+1,len(v)):
                if v[k]==v[i]:
                    c+=1
            l.append((v[i],c))
    return l

# Coste temporal

# Se supone que len(v), v[i] y l.append((v[i],c)) tienen coste fijo

# r es el número de iteraciones del primer while: entre 1 y len(l)

# d(i) = 0 si v[i] ya ha sido visto antes
# d(i) = 1 si v[i] no ha sido visto antes

# t(n) = 1 + Suma{i=0-->n-1} (1 + r + d(i) * (1 + Suma{k=i+1-->n-1} (1) + 1) + 1
#      = 2 + Suma{i=0-->n-1} (1 + r + d(i) * (n-i+1))

# Mejor caso: v contiene un único valor repetido len(v) veces.
# En este caso, r=1, d(0)=1 y d(i)=0 para i>0.
# Nos queda:

# tm(n) = 2 + (n+3) + Suma{i=1-->n-1} (2) = 2 + (n+3) + 2*(n-1) = 3*n + 3
# tm(n) esta en Theta(n)

# Peor caso: v contiene len(v) valores distintos.
# En este caso, r = len(l) + 1 = i + 1 y d(i) = 1 para todo i.
# Nos queda:

# tp(n) = 2 + Suma{i=0-->n-1} (1 + i + 1 + (n-i+1))
#       = 2 + Suma{i=0-->n-1} (n+3) = 2 + n*(n+3) = n^2 + 3*n + 2
# tp(n) esta en Theta(n^2)

# Así pues:
# t(n) está en Omega(n)
# t(n) está en O(n^2)


def reps_diccionario(v):  # n = len(v)
    d = {}
    for i in range(len(v)):
        d[v[i]] = d.get(v[i],0) + 1
    return list(d.items())

# Coste temporal

# Se supone que len(v), v[i] y d.get() tienen coste fijo
# d.items() tiene un coste lineal con el número de elementos del diccionario
# list(iterable) tiene un coste lineal con el número de elementos del iterable

# t(n) = 1 + Suma{i=0-->n-1} (1) + 2*D = 1 + n + 2*D

# En la expresión anterior, D es el número de elementos distintos en v

# Mejor caso: D=1
# tm(n) = n + 3

# Peor caso: D=n
# tp(n) = 3*n + 1

# Así pues, t(n) está en Theta(n)

import random
import time

RANGO_VALORES = 10000   # ~ peor caso

# RANGO_VALORES = 10    # ~ mejor caso

# Cuanto mayor sea RANGO_VALORES, más improbable es que haya
# dos elementos iguales y, por tanto, mayor será la diferencia
# entre los costes de las dos versiones (porque estaremos
# más cerca del peor caso en la primera versión).

fs="{0:>15s} {1:>15s} {2:>15s}"
print(fs.format("Tamaño","t(Lista)","t(Dicc)"))
for n in [10,100,1000,10000]:
    l=[random.randint(1,RANGO_VALORES) for i in range(n)]
    t1=time.process_time()
    reps_lista(l)
    t2=time.process_time()
    reps_diccionario(l)
    t3=time.process_time()
    fs="{0:>15d} {1:>15.4e} {2:>15.4e}"
    print(fs.format(n,t2-t1,t3-t2))

