#EX1
def createHisto(file):
    f = open(file)
    doc = f.read()
    d = dict()
    for words in doc.split():
        if words in d:
            d[words] = d[words] + 1
        else:
            d[words] = 1
    f.close()
    return d
    
def returnWords(histo,n):
    ls = []
    for i in histo.keys():
        if n == histo[i]:
            ls = ls + [i]
    return ls

#EX2
def returnInverseD(file):
    d = createHisto(file)
    trueD = dict()
    for elements in d.keys():
        if d[elements] not in trueD:
            trueD[d[elements]] = [elements]
        else:
            trueD[d[elements]] = trueD[d[elements]] + [elements]
    return trueD

#EX3
def locateWords(file):
    with open(file) as f:
        text = f.readlines()
        d_pos = dict()
        i = 1
        for lines in text:
            j = 1
            for words in lines.split():
                if words not in d_pos:
                    d_pos[words] = [(i,j)]
                else:
                    d_pos[words] = d_pos[words] + [(i,j)]
                j += 1
            i += 1
    return d_pos

#EX4
def factorOut(n):
    dFac = dict()
    i = 2
    while n > 1:
        if n%i == 0:
            n = n/i
            if i not in dFac:
                dFac[i] = 1
            else:
                dFac[i] = dFac[i] + 1
        else:
            i += 1
    return dFac

#EX5
def shelveDic(file,n):
    import shelve
    dFac = shelve.open(file)
    i = 2
    while n > 1:
        if n%i == 0:
            n = n/i
            i = str(i)
            if i not in dFac:
                dFac[i] = 1
            else:
                dFac[i] = dFac[i] + 1
        else:
            i += 1
        i = int(i)
    dFac.close()
    
def readShelve(file1,file2):
    import shelve
    f1 = shelve.open(file1)
    f2 = shelve.open(file2)
    maxV = 1
    for e1 in f1:
        for e2 in f2:
            if e1 == e2:
                if int(e1) > maxV:
                    maxV = int(e1)
    f1.close()
    f2.close()
    return maxV

#EX6.
#vamos a suponer que son matrices cuadradas para simplificar, además del hecho de que debemos multiplicarlas
def osatuMatrix(target):
    coord = list(target.keys())
    size = coord[len(coord)-1][0]
    mat = []
    for i in range(size + 1):
        mat.append([0])
        for j in range(size + 1):
            if (i,j) == coord[i]:
                mat[i][j] = target[coord[i]]
            else:
                mat[i].append(0)
    return mat

def sumaMat(mat1, mat2):
    matDef = []
    for i in range(len(mat1)):
        matDef.append([])
        for j in range(len(mat1)):
            matDef[i].append(mat1[i][j] + mat2[i][j])
    return matDef

def timesMat(mat1,mat2):
    matDef = []
    for i in range(len(mat1)):
        matDef.append([])
        for j in range(len(mat1)):
            add = 0
            for z in range(len(mat1)):
                add = add + mat1[i][z]*mat2[z][j]
            matDef[i].append(add)
    return matDef
