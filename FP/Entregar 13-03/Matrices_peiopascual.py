#EX1
def randomMat(n,m):
    import random
    mat = []
    for i in range(n):
        mat.append([])
        for j in range(m):
            mat[i].append(int(random.choice("0123456789")))
    return mat

#EX2
def storeMatrix(mat, file):
    with open(file, mode="a") as f:
        for i in range(len(mat)):
            for j in range(len(mat[i])):
                f.write(str(mat[i][j]) + "\t")
            f.write("\n")
#EX3
def recoverMatrix(file):
    with open(file, mode="r") as f:
        mat = f.readlines()
        m = len(mat)
        n = len(mat[0].split())
        matDef = []
        for i in range(m):
            matDef.append(mat[i].split())
        return matDef
#EX4
def getTraza(mat):
    suma = 0
    for i in range(len(mat)):
        suma = suma + int(mat[i][i])
    return suma
#EX5
def getTrasp(mat):
    matDef = [[ 0 for i in range(len(mat))]for i in range(len(mat))]
    for j in range(len(mat)):
        for i in range(len(mat)):
            print(matDef)
            print(" ")
            matDef[j][i] = mat[i][j]
    return matDef
#EX6
def sumaMat(mat1, mat2):
    matDef = []
    for i in range(len(mat1)):
        matDef.append([])
        for j in range(len(mat1)):
            matDef[i].append(mat1[i][j] + mat2[i][j])
    return matDef
#EX7
def timesMat(mat1,mat2):
    matDef = []
    for i in range(len(mat1)):
        matDef.append([])
        for j in range(len(mat1)):
            add = 0
            for z in range(len(mat2)):
                add = add + mat1[i][z]*mat2[z][j]
            matDef[i].append(add)
    return matDef
