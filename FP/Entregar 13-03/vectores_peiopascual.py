#EX1
def normaVector(vector):
    norma = 0
    for comps in vector:
        norma = norma + comps**2
    return norma**(1/2)

#EX2
def scalarVector(vector, scalar):
    for pos in range(len(vector)):
        vector[pos] = vector[pos] * scalar
    return vector

#EX3
def scalarProduct(v1, v2):
    escRes = 0
    for pos in range(len(v1)):
        escRes = escRes + v1[pos]*v2[pos]
    return escRes

#EX4
def proyectVector(v1,v2):
    return [normaVector(v1) * v2[i]/normaVector(v2) for i in range(len(v1))]

#EX5
def angleVector(v1,v2):
    from math import acos
    return acos(scalarProduct(v1,v2)/(normaVector(v1)*normaVector(v2)))
    
#EX6
def sumaVector(v1,v2):
    return [v1[i] + v2[i] for i in range(len(v1))]
