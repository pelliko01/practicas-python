#EX1.
def getMax(f, values):
    n = f(values[0])
    x = values[0]
    for i in range(values[0],values[1] + 1):
        m = f(i)
        if m > n:
            n = m
            x = i
    return (n,x)

#EX2
def translateNum(num):
    if num[0] != "-":
        s = num[0]
        i = len(num)-1
        x = int()
        p = 0
        if s == "b":
            while i >= 1:
                x = x + int(num[i])*(2**p)
                p = p + 1
                i = i - 1
            return x
        elif s == "d":
            while i >= 1:
                x = x + int(num[i])*(10**p)
                p = p + 1
                i = i - 1
            return x
        elif s == "o":
            while i >= 1:
                x = x + int(num[i])*(8**p)
                p = p + 1
                i = i - 1
            return x
        else:
            while i >= 1:
                x = x + int("0123456789abcdef".find(str(num[i])))*(16**p)
                p = p + 1
                i = i - 1
            return x
    else:
        s = num[1]
        i = len(num)-1
        x = int()
        p = 0
        if s == "b":
            while i >= 2:
                x = x + int(num[i])*(2**p)
                p = p + 1
                i = i - 1
            return -x
        elif s == "d":
            while i >= 2:
                x = x + int(num[i])*(10**p)
                p = p + 1
                i = i - 1
            return -x
        elif s == "o":
            while i >= 2:
                x = x + int(num[i])*(8**p)
                p = p + 1
                i = i - 1
            return x
        else:
            while i >= 2:
                x = x + int("0123456789abcdef".find(str(num[i])))*(16**p)
                p = p + 1
                i = i - 1
            return -x

#EX3. 
def createMatrix(file):
    with open(file) as f:
        mat = f.readlines()
        res = []
        i = 0
        j = 0
        for elements in mat:
            elements = elements.split()
            if i < int(elements[0]):
                i = int(elements[0])
            if j < int(elements[1]):
                j = int(elements[1])
        for a in range(0, i + 1):
            res.append(list())
            add = []
            for b in range(0, j + 1):
                add.append(0)
            res[a] = add
        for elements in mat:
            elements = elements.split()
            res[int(elements[0])][int(elements[1])] = float(elements[2])
            
#EX4.
def duplicateList(ls):
    ls2 = []
    for elements in ls:
        if elements not in ls2:
            if ls.count(elements) > 1:
                ls2.append(elements)
    return ls2

#EX5.
def writeTuples(ls):
    ls2 = []
    lsN = str()
    for elements in ls:
        lsN = lsN + str(elements)
    for elements in lsN:
        if (int(elements),ls.count(elements)) not in ls2:
            ls2.append((int(elements),ls.count(elements)))
    return ls2
