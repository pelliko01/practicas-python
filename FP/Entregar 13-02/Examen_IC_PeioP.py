#Peio Pascual Hernando

#EX1.
#a) código corregido:
cadena = "hola" + str(6)
cadena = list(cadena)
cadena[4] = 8
print(cadena)
#Cambios: pasar a tipo string el 6, convertir en lista la variable cadena para que la asignación cadena[4] tenga sentido, y finalmente usar índice 4 en lugar de 5 para dicha asignación
#Resultado: >>> hola8

#b) código corregido:
cadena = "numero: 2 4 7 9"
em = 0
for z in cadena.split()[1:]:
    z = int(z)
    if z % 2 == 0:
        em += z
print(em)
#Cambios: obligatorio empezar por el segundo término de la lista para evitar un typeError, convierte el miembro de la cadena a entero para poder hacer el módulo y la suma
#Resultado: >>> 6

#c)código corregido
def func(x):
    return x-1
x = 4
print(func(x)*func(3*x))
print(func(2)*func(2))
#Cambios: en lugar de a y b, se han introducido valores. Si se quiere usar a y b primero habría que definirlas.
###################################

#EX2.
#a) código corregido
def func(nombre):
    fp = open(nombre, "r")
    doc = fp.readlines()
    i = 0
    for elements in doc:
        doc[i] = elements.split()
        i += 1
    fp.close()
    fp2 = open(nombre[:len(nombre)-4]+"2.txt","a")
    for lines in doc:
        me = 0
        for i in range(1, len(lines)):
            me+= int(lines[i])
        me/=len(lines)-1
        fp2.write(lines[0] + " " + str(me) + "\n")
    fp2.close()
        
    fp2.close()
#Reultado: no muestra nada por pantalla, pero crea un archivo (si no existe ya) y escribe el nombre de cada alumno seguido de la media de sus notas.

#b) Si el archivo se abre en modo "añadir", no se borran los datos ya existentes
###################################

#EX3.
#a)
def printNumbers(ls1, ls2):
    for i in range(len(ls1)):
        print(ls1[i], ls2[i])
def printNumbersF(f1, f2):
    doc1 = open(f1, "r")
    doc2 = open(f2, "r")
    ls1 = doc1.readlines()
    ls2 = doc2.readlines()
    doc1.close()
    doc2.close()
    for i in range(len(ls1)):
        print(ls1[i].split()[0], ls2[i].split()[0])
def printNumbersD(ls1, ls2):
    Dic = {}
    for i in range(len(ls1)):
        Dic[ls1[i]] = ls2[i]
    return Dic
def printNumbersD2(f1, f2):
    doc1 = open(f1, "r")
    doc2 = open(f2, "r")
    ls1 = doc1.readlines()
    ls2 = doc2.readlines()
    doc1.close()
    doc2.close()
    Dic = {}
    for i in range(len(ls1)):
        add = ls1[i].split()[0]
        if add not in Dic:
            Dic[add] = ls2[i].split()
        else:
            Dic[add] = Dic[add] + ls2[i].split()
    return Dic
###################################

#EX4.
#a)
#Resultado: >>>{"a": 5, "b":4, "c": 0} El código lee una serie de tuplas y elabora un diccionario cuyas claves son letras (el primer miembro de la tupla) y los valores la suma acumulada de los segundos miembros de la tupla 

#b)
fp = open("test02.txt")
doc = fp.read()
print(func(sacar_valores(doc)))
fp.close()

#EX5.
def checkMatrix(mt):
    maxi = int()
    maxiC = int()
    locationF = int()
    locationC = int()
    for elements in mt:
        if len(mt[0]) != len(elements):
            return [], []
    for i in range(len(mt)):
        mini = int()
        for j in range(len(mt[0])):
            mini = mini + mt[i][j]
        if maxi < mini:
            maxi = mini
            locationF = i
    for j in range(len(mt[0])):
        mini = int()
        for i in range(len(mt)):
            mini = mini + mt[i][j]
        if maxiC < mini:
            maxiC = mini
            locationC = j
    return mt[locationF], [mt[i][locationC] for i in range(len(mt))]
