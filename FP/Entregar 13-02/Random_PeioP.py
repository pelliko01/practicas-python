import random
#EX1.
def returnWord(n):
    word = ""
    for i in range(n):
        if i%2 == 0:
            word = word + random.choice("aeiou")
        else:
            word= word + random.choice("qwrtypsdfghjklñzxcvbnm")
    return word

#EX2.
def randomWords(file, n):
    f = open(file, "r")
    doc = f.readlines()
    f.close()
    seq = ""
    print(doc, "\n")
    for i in range(n):
        seq = seq + random.choice(doc)
    return seq

#EX3.
def simDados():
    return (random.choice("123456"),random.choice("123456"))

#EX4.
def gaussianF(x,v,n):
    return [random.gauss(x,v**0.5) for i in range(n)]

#EX5.
def nPermutation(k,n):
    nums = [i for i in range(1,n+1)]
    print(nums)
    for i in range(k):
        random.shuffle(nums)
        print(nums)

