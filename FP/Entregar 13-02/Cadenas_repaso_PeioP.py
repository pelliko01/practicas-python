import string
#EX1.
def isInStr(s1,s2):
    for chars in s1:
        if chars in s2:
            if s2.count(chars) >= 2:
                return False
            else:
                return True
        else:
            return True
#EX2.
def swap_pos(s,l):
    n = int(len(l)/2)
    s = list(s)
    nS = ""
    for i in range(n):
        a = s[l[2*i]]
        b = s[l[2*i + 1]]
        s[l[2*i]] = b
        s[l[2*i+ 1]] = a
    for j in s:
        nS = nS + j
    return nS

#EX3.
def lrot(s,n):
    alfa = string.ascii_lowercase
    nS = ""
    for elements in s:
        nS = nS + alfa[(s.find(elements) - n)%len(alfa)]
    return nS
def rrot(s,n):
    alfa = string.ascii_lowercase
    nS = ""
    for elements in s:
        nS = nS + alfa[(s.find(elements) + n)%len(alfa)]
    return nS

#EX4.
def find_all(t,s,overlaps=True):
    if overlaps == True:
        rep = 0
        for i in range(len(t)):
            rep = rep + s[i:].count(t)
    else:
        rep = s.count(t)
    return rep

print(find_all("abb","abbbbabbababababb"))
print(find_all("bbb","abbbbabbababababb"))
print(find_all("aba","abbbbabbababababb"))

#EX5.
def my_split(s, sep_seq, max_splits):
    ls = []
    if sep_seq == None:
        for i in range(max_splits):
            ls = s.split()
        return ls
    else:
        for i in range(max_splits):
            ls = s.split(sep_seq)
        return ls
