#Peio Pascual Hernando

import math
import random
import punto #necesario para los ejercicios 2 y 3
#1. Clase conjunto
class Conjunto:
    def __init__(self):
        self.__elements = []
        self.__card = 0
        self.__type = None
        
    def tipo(self):
        if self.__card == 0:
            return None
        else:
            return self.__type
        
    def addElem(self,e):
        if self.__card == 0 or self.__type == type(e):
            if e not in self.__elements:
                self.__elements.append(e)
                self.__card = self.__card + 1
                self.__type = type(e)
        
    def delElem(self,e):
        if e in self.__elements:
            nL = []
            for elements in self.__elements:
                if elements != e:
                    nL.append(elements)
            self.__elements = nL
            self.__card = self.__card - 1

    def __contains__(self,e):
        return e in self.__elements
    
    def __iter__(self):
        return iterConj(self.__elements)

    def __len__(self):
        return self.__card
        
    def randExt(self):
        self.__elements.pop(random.choice(range(0,self.__card)))
        
    def vaciar(self):
        self.__elements = []

    def __eq__(self,other):
        if self.__card != other.__card:
            return False
        else:
            for i in range(self.__card):
                if self.__elem[i] != other.__elem[i]:
                    return False
            return True
    def copia(copia, self):
        copia.__elements = self.__elements
        copia.__card = self.__card
        copia.__type = self.__type
        return copia

    def __or__(self,other): #unión |
        if self.__type == other.__type:
            union = Conjunto()
            union.copia(self)
            for i in other:
                union.addElem(i)
            return union
        else:
            raise TypeError
        
    def __and__(self,other): #intersección & 
        if self.__type == other.__type:
            inter = Conjunto()
            for i in self:
                if i in other:
                    inter.addElem(i)
            return inter
        else:
            raise TypeError

    def __sub__(self,other): #resta -
        if self.__type == other.__type:
            for i in other.__elements:
                self.delElem(i)
            return self
        else:
            raise TypeError
            
    def __xor__(self,other): #resta simétrica ^
        if self.__type == other.__type:
            simD = Conjunto()
            for i in self:
                if i not in other:
                    simD.addElem(i)
            for j in other:
                if j not in self:
                    simD.addElem(j)
            return simD            
        else:
            raise TypeError

    def esSubc(self,other):
        if self.__type == other.__type:
            for i in self.__elements:
                if not other.__contains__(i):
                    return False
            return True
        else:
            raise TypeError

    def esSuperc(self,other):
        if self.__type == other.__type:
            for i in other.__elements:
                if not self.__contains__(i):
                    return False
            return True
        else:
            raise TypeError
        
    def __print__(self):
        print(self.__elements)
        
class iterConj:
    def __init__(self,elemList):
        self.__elements = elemList
        self.__index = 0
            
    def __iter__(self):
        return self
    
    def __next__(self):
        if self.__index < len(self.__elements):
            elem = self.__elements[self.__index]
            self.__index += 1
            return elem
        else:
            raise StopIteration

#2. clase Triangulo ///////////////////////////////////////////////////////////////////////////
class Triangulo():
    def __init__(self,p1 = Punto(round(random.random()*100,2),round(random.random()*100,2), "p1"),p2 = Punto(round(random.random()*100,2),round(random.random()*100,2), "p2"),p3 = Punto(round(random.random()*100,2),round(random.random()*100,2), "p3")):
        self.__p1 = p1
        self.__p2 = p2
        self.__p3 = p3
    def copia(self):
        copia = Triangulo()
        copia.__p1 = self.__p1
        copia.__p2 = self.__p2
        copia.__p3 = self.__p3
        
    def escalar(self,r,p):
        if p == self.__p1:
            self.__p2.scale(r)
            self.__p3.scale(r)
        elif p == self.__p2:
            self.__p1.scale(r)
            self.__p3.scale(r)
        elif p == self.__p3:
            self.__p1.scale(r)
            self.__p2.scale(r)

    def trasladar(self,p):
        d = [p1.euclidea(p),p2.euclidea(p), p3.euclidea(p)]
        i = d.index(min(d))
        if i == 1:
            self.__p1 = p
        elif i == 2:
            self.__p2 = p
        else:
            self__p3 == p
        
    def lsVertices(self):
        return [self.__p1,self.__p2,self.__p3]
    
    def esVertice(self,p):
        if p == self.__p1:
            return True
        elif p == self.__p2:
            return True
        elif p == self.__p3:
            return True
        else:
            return False
        
    def lados(self):
        a = p1.euclidea(p2)
        b = p1.euclidea(p3)
        c = p2.euclidea(p3)
        return [a,b,c]

    def vector(p1,p2):
        return (p2[0] - p1[0],p2[1] - p1[1])
    
    def angulos(self):
        sides = self.lados()
        aV = (vector(self.__p1,self.__p3), vector(self.__p1,self.__p2))
        bV = (vector(self.__p1,self.__p2), vector(self.__p3,self.__p2))
        cV = (vector(self.__p3,self.__p1), vector(self.__p2,self.__p3))
        a = math.acos((aV[0][0]*aV[1][0]+aV[0][1]*aV[1][1])/(sides[1]*sides[2]))
        b = math.acos((bV[0][0]*bV[1][0]+bV[0][1]*bV[1][1])/(sides[0]*sides[3]))
        c = math.acos((cV[0][0]*cV[1][0]+cV[0][1]*cV[1][1])/(sides[0]*sides[1]))
        return ([a,b,c])
    def __eq__(self, other): #definido como tres lados de igual longitud
        s1 = self.lados()
        s2 = other.lados()
        for k in s1:
            try:
                s2.remove(k)
            except ValueError:
                return False
        return True
            
    def area(self):
        sides = self.lados()
        p = sum(sides)/2
        return math.sqrt(p*(p-sides[0])*(p-sides[1])*(p-sides[2]))

    def esIsosceles(self):
        sides = self.lados()
        if self.esEquilatero():
            return False
        elif sides[0] == sides[1] or sides[0] == sides[2] or sides[1] == sides[2]:
            return True
        return False
    
    def esEquilatero(self):
        sides = self.lados()
        if sides[0] == sides[1] and sides[0] == sides[2]:
            return True

    def esRectangulo(self):
        angulos = self.angulos()
        if 90 in angulos:
            return True
        return False
        
#3. Puntos en el plano ///////////////////////////////////////////////////////////////////////////
def randomPoints(n):
    points = Conjunto()
    for i in range(n+1):
        points.addElem(Punto(round(random.random()*100,2),round(random.random()*100,2), "p" + str(i)))
    return points

def longerDistance(puntos):
    maxDist = 0
    for i in puntos:
        for j in puntos:
            if int(j.get_label()[1:]) <= int(i.get_label()[1:]): #esto es necesario para evitar que la funcion compare puntos ya comparados (hay soluciones más elegantes)
                pass
            else:
                currentD = i.euclidea(j)
                if currentD > maxDist:
                    maxDist = currentD
                    inter = (i,j)
    return maxDist, inter

def centroid(puntos):
    distCollection = dict()
    minD = 1420 #aproximacion a 10 veces raíz de 20000, que es la distancia máxima teórica para 10 puntos (con otro número de puntos y distinto límite superior esto cambia)
    for i in puntos:
        currentD = 0
        for j in puntos:
            currentD += i.euclidea(j)
        if currentD < minD:
            minD = currentD
            minP = i
    return minD, minP

