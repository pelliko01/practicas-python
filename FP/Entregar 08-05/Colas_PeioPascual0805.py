#1. Cola de un solo atributo

class Cola(object):
    def __init__(self, Max = 50):
        self.__elems = [0] * (Max + 2)
        
    def __len__(self):
        return self.__elems[-1]

    def isFull(self):
        if self.__elems[-1] == len(self.__elems) - 2:
            return True
        return False
    def isEmpty(self):
        if self.__elems[-1] == 0:
            return True
        return False
        
    def enqueue(self,add):
        if self.isFull():
            return False
        elif self.isEmpty():
            self.__elems[0] = add
            self.__elems[-1] = 1
            self.__elems[-2] = 0
        else:        
            self.__elems[(self.__elems[-2] + self.__elems[-1])%(len(self.__elems)-2)] = add
            self.__elems[-1] += 1
            return True
        
    def dequeue(self):
        if self.isEmpty():
            return False
        else:
            rem = self.__elems[self.__elems[-2]]
            self.__elems[self.__elems[-2]] = 0
            self.__elems[-2] += 1
            self.__elems[-1] -= 1
            return rem
    def doTell(self):
        return self.__elems
        
#2. Cola doble
class Cola_Doble(object):
    def __init__(self, Max = 50):
        self.__elems = [0] * (Max)
        self.__head = 0
        self.__lF = 0
        self.__rF = 0
        self.__tail = 0
        self.__nelems = 0
        
    def __len__(self):
        return self.__nelems

    def isFull(self):
        if self.__nelems == len(self.__elems) - 1:
            return True
        return False
    
    def isEmpty(self):
        if self.__nelems == 0:
            return True
        return False
    
    def leftEnq(self, add):
        if self.isEmpty():
            self.__elems[0] = add
            self.__head -= 1
            self.__nelems += 1
        elif not self.isFull():
            self.__elems[(self.__head-1)%len(self.__elems)] = add
            self.__head -= 1
            self.__nelems += 1
        else:
            return False
        
    def rightEnq(self,add):
        if self.isEmpty():
            self.__elems[0] = add
            self.__tail -= 1
            self.__nelems += 1
        elif not self.isFull():
            self.__elems[(self.__tail)%len(self.__elems)] = add
            self.__tail += 1
            self.__nelems += 1
        else:
            return False








