class Pila:
    def __init__(self, Max = 50):
        self.__elems = [None] * Max
        self.__top = Max
        self.__size = Max
        
    def push(self, add):
        if self.__top > 0:
            self.__top -= 1
            self.__elems[self.__top] = add
            return True
        else:
            return False
        
    def pop(self):
        if len(self.__elems) != 0:
            ext,self.__elems[self.__top] = self.__elems[self.__top], None
            self.__top += 1
            return ext
        else:
            return False

    def top(self):
        if self.__top < self.__size:
            return self.__elems[self.__top]
        else:
            return "#"

    def len(self):
        return self.__size - self.__top

    def isEmpty(self):
        if self.__top == self.__size:
            return True
        else:
            return False
            
#1 retornar expresión postfija
    def convPostfija(self,expresion):
        operator = "*/+-("
        operand = "1234567890"
        postExp = ""
        for els in expresion:
            if els in operand or els == "(":
                self.push(els)
            elif els == ")":
                while self.top() != "(":
                    postExp += self.pop()
                self.pop()
            elif els in operator:
                while self.isEmpty() == False:
                    if self.top() in operand:
                        postExp += self.pop()
                    elif operator.index(self.top()) <= operator.index(els):
                        postExp += self.pop()
                    else:
                        break
                self.push(els)
        while self.isEmpty() ==False:
            postExp += self.pop()
        return postExp
#2 evaluar expresión postfija
    def evalPostfija(self, expresion):
        operator = "*/+-("
        operand = "1234567890"
        postExp = ""
        for els in expresion:
            if els in operand:
                self.push(els)
            else:
                b, a = self.pop(), self.pop()
                res = eval(str(a) + els + str(b))
                self.push(res)
        return self.pop()
s = Pila()
ex = input("Introduzca su expresión infija: ")
print("El resultado es:",s.evalPostfija(s.convPostfija(ex)))
