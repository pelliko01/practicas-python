#Peio Pascual Hernando y David Cuenca Marcos
from os import system, name
#función principal train:
def trainSystem(file): #lee el archivo de train (análoga a testsystem), posteriormente lee las direcciones de cada clase y envía a la purga cada email
    import time, random
    t1 = time.process_time()
    f = open(file)
    doc1 = f.read().split()
    f.close()
    classLs = list()
    for address in doc1:
        histoCompiled = dict()
        g = open(address)
        doc2 = g.read().split()
        g.close()
        for email in doc2:
            emailCleaned = purgeText(email)
            histoSingle = createHisto(emailCleaned)
            histoCompiled = compileHisto(histoSingle,histoCompiled)
        classLs.append(histoCompiled)
    classLs = commonCompiled(classLs)
    for i in range(len(classLs)):
        classLs[i] = normalHisto(classLs[i])
    writeHisto(classLs)
    t2 = time.process_time()
    print('Entrenamiento concluído en {0:.4e} segundos.'.format(t2-t1))
        
def purgeText(file): #función que elimina caracteres indeseados de los emails
    f = open(file)
    doc = f.readlines()
    f.close()
    jump = False
    global d
    d = dict()
    d.clear()
    docDef = list()
    for lines in doc:
        if not jump:
            if lines == "\n":
                jump = True
        else:
            add = ""
            for words in lines:
                words = words.strip(",.;:~-=+^?()«»#@%!1234567890¬|<>\/``·[]''{}*!ç&$_")
                words = words.strip('""')
                words = words.lower()
                add = add + words
            docDef.append(add)
    return docDef

def createHisto(docDef): #crea un histograma de palabras de cada email
    for lines in docDef:
        for words in lines.split():
            if words not in d:
                d[words] = 1
            else:
                d[words] = d[words] + 1
    return d

def compileHisto(histo,histoTarget): #recopila los histogramas de cada email en uno único para cada clase de email
    for keys in histo.keys():
        if keys not in histoTarget:
            histoTarget[keys] = histo[keys]
        else:
            histoTarget[keys] = histoTarget[keys] + histo[keys]
    return histoTarget

def normalHisto(histo): #normaliza los histogramas
    sumaT = sum(list(histo.values()))
    for keys in histo:
        histo[keys] = histo[keys]/sumaT
    return histo

def commonCompiled(lsAll): #elimina las palabras muy poco comunes y las muy comunes de cada histograma compilado, usando un histograma que compila todos los histogramas
    macroHisto = {}
    sumaT = 0
    for histo in lsAll:
        for keys in histo:
            if keys not in macroHisto:
                macroHisto[keys] = histo[keys]
            else:
                macroHisto[keys] = macroHisto[keys] + histo[keys]
    sumaT = (sum(macroHisto.values())//len(macroHisto))
    j = 0
    for histo in lsAll:
        while j < len(histo.keys()):
            element = list(histo.keys())[j]
            if histo[element] >= sumaT or histo[element] < 3:
                del histo[element]
            else:
                j += 1
    return lsAll

def writeHisto(lsAll): #escribe el histograma en un archivo 
    with open("result.txt", mode = "a") as f:
        for histo in lsAll:
            f.write(str(histo))
            f.write("//////////////////////")

#función principal test:
def testSystem(file): #lee el archivo de test (análoga a trainsystem), posteriormente lee las direcciones de cada clase y envía a la purga cada email
    global parameterK
    global parameterDist
    f = open(file)
    doc1 = f.read().split()
    global universalKeys
    i = 0
    for elements in doc1:
        universalKeys[i] = elements
        i = i + 1
    f.close()
    for address in doc1:
        print(address[12:-4])
        aciertos = 0
        g = open(address)
        doc2 = g.read().split()
        total = len(doc2)
        g.close()
        for email in doc2: #para facilitar la lectura cada acción sobre el texto/email original tiene asociado un nombre de variable
            emailCleaned = purgeText(email)
            histoTarget = createHisto(emailCleaned)
            histoNormal = normalHisto(histoTarget)
            if parameterDist == "correl":
                if howCorrect(address,correlationMail(histoNormal),parameterK) == True:
                    aciertos += 1
            elif parameterDist == "super":
                if howCorrect(address,superposeMail(histoNormal),parameterK) == True:
                    aciertos += 1
            if parameterDist == "euclid":
                if howCorrect(address,euclidMail(histoNormal),parameterK) == True:
                    aciertos += 1
            else:
                if howCorrect(address,superposeMail(histoNormal),parameterK) == True:
                    aciertos += 1
        print(" *Porcentaje de error: {0:5.4f}".format(100*(total - aciertos) / total))

def howCorrect(address, checkList,k):
    for i in range(k):
        if address == universalKeys[checkList.index(min(checkList))]:
            return True
        else:
            checkList.pop(checkList.index(min(checkList)))
    return False
    
def superposeMail(histo):
    dCollection = []
    for histos in masterHisto:
        suma = 0
        for keys in histo:
            suma = suma + min(histo[keys],histos.get(keys,0))
        suma = 1 - suma
        dCollection.append(suma)
    return dCollection

def euclidMail(histo):
    dCollection = []
    for histos in masterHisto:
        dist = 0
        for keys in histo:
            dist = dist + (histo[keys] - histos.get(keys,0))**2
        dist = dist**0.5
        dCollection.append(dist)
    return dCollection

def correlationMail(histo):
    dCollection = []
    for histos in masterHisto:
        suma = 0
        for keys in histo:
            suma = suma + (histo[keys] * histos.get(keys,0))
        suma = 1 - suma
        dCollection.append(suma)
    return dCollection

def loadDatabase(file): #nuestra RAM lo puede soportar así que iremos por esta vía
    global masterHisto
    with open(file) as f:
        masterHisto = f.read().split("//////////////////////")[:-1]
    for i in range(len(masterHisto)):
        masterHisto[i] = eval(masterHisto[i])

def chooseParameters():
    global parameterK
    global parameterDist
    parameterK = int(input("Introduzca el numero de clases mas proximas permitidas(parametro K): "))
    parameterDist = input("Introduzca 'euclid' para calcular la distancia euclidea, 'superpos' para la de superposicion o 'correl' para la de correlacion (por defecto, correlación): ")
    print(parameterDist)
    print('{0:15s}'.format("//////////////////////"))

#——————————MAIN——————————#           
global masterHisto
global universalKeys
global d
global parameterDist
global parameterK
masterHisto = list()
universalKeys = dict()
import time
## Ejecución de la interface
system('cls' if name == 'nt' else 'clear')
for line in ["## PRACTICA 1: CLASIFICACIÓN DE EMAIL ##", "# Por Peio Pascual y David Cuenca #"]:
    print("{0:^80s}".format(line))
    time.sleep(0.25)

choose = input("Introduce 'e' para entrenar o 'v' para evaluar: ")
if choose == "e":
    trainSystem("metalista_train.txt")
elif choose == "v":
    try:
        loadDatabase("result.txt")
    except:
        class MissingDatabaseError (Exception):
            pass
        from os import getcwd, listdir
        from sys import exit
        raise MissingDatabaseError("No database was found in {}".format(getcwd()), 'Actual working path content: '+' ,'.join(listdir()))
    print('\n')
    chooseParameters()
    print('\n')
    testSystem("metalista_test.txt")
else:
    class RecognitionError(Exception):
        pass
    raise RecognitionError("No available option was chosen")
