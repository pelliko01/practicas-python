#1. 
class ColaPriA:
    def __init__(self, priF, n = 100):
        self.__size = n
        self.__priF = priF
        self.__elems = [None] * self.__size
        self.__pri = 0
        self.__ulti = -1
        self.__nE = 0
        
    def isFull(self):
        if self.__nE != len(self.__elems):
            return False
        return True

    def isEmpty(self):
        if self.__nE == 0:
            return True
        return False
    
    def enqueue(self, add):
        if not self.isFull():
            if not self.isEmpty() and (self.__ulti + 1)%self.__size == self.__pri:
                self.compact()
            self.__ulti = (self.__ulti + 1) % self.size
            self.__elems[self.__ulti] = add
            return True
        return False
        
    def len(self):
        return self.__nE
    
    def dequeue(self):
        if self.isEmpty():
            raise RuntimeError("Error: cola vacía")
        pMax = None
        i = self.__pri
        n = 0
        while n < self.__nE:
            if self.__elems[i] == None:
                i = i + 1
            else:
                if pMax == None or pMax < self.__priF(self.__elems[i]):
                    pMax = self.__priF(self.__elems[i])
                    posMax = i
                i = i + 1
                n = n + 1
        ext, self.__elems[posMax] = self.__elems[posMax], None
        self.__nE = self.__nE - 1
        return ext

    def compact(self):
        r, w = (self.__pri + 1) % self.__size
        while r != self.__pri + self.__nE:
            if self.__elems[r] != None:
                self.__elems[w] = self.__elems[r]
                w += 1
                r += 1
                continue
            r += 1
        self.__ulti = r
        return None
        
#2.
class ColaPriB():
    def __init__(self, fPri, Max = 100):
        self.__size = Max
        self.__elems = [None] * self.__size
        self.__nE = 0
        self.__pr = 0
        self.__ul = -1
        self.__fPri = fPri

    def len(self):
        return self.__nE

    def isEmpty(self):
        if self.__nE == 0:
            return True
        return False

    def isFull(self):
        if self.__nE == self.__size:
            return True
        return False

    def enqueue(self, add):
        if self.isFull():
            return False
        else:
            i = self.__pr
            while self.__fPri(self.__elems[i]) >= self.__fPri(add):
                i = (i + 1) % self.__size
            while i != self.__ul:
                move = self.__elems[i]
                self.__elems[i] = add
                i = (i + 1) % self.__size
                add = move
                if self.__elems [i] == None:
                    break
            self.__ul = (self.__ul + 1) % self.__size
            self.__nE +=1

    def dequeue(self):
        if self.isEmpty():
            return False
        ext, self.__elems[self.__pr] = self.__elems[self.__pr], None
        self.__nE -= 1
        self.__pr = (self.__pr + 1) % self.__size
        return ext




















        
