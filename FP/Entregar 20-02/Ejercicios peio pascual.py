#Peio Pascual Hernando

#EX1.
def writeForbidden(file1, file2,forbidden):
    f1 = open(file1, mode = "r")
    f2 = open(file2, mode = "w")
    doc1 = f1.readlines()
    i = 0
    add = []
    for lines in doc1:
        for words in lines.split():
            jump = False
            for char in words:
                if char in forbidden:
                    jump = True
            if not jump:
                add.append(words)
        f2.write(str(i) + ":" + str(add) + "\n")
        add = []
        i += 1
    f1.close()
    f2.close()
#writeForbidden("test1.txt","test2.txt","a")

#EX2.
def writeAllowed(file1, file2,allowed):
    f1 = open(file1, mode = "r")
    f2 = open(file2, mode = "w")
    doc1 = f1.readlines()
    i = 0
    j = 0
    perc = int()
    for lines in doc1:
        for words in lines.split():
            count = True
            for char in words:
                if char not in allowed:
                    count = False
                    break
            if count:
                j = j + 1
            print(words,j)
        perc = 100 * j / len(lines.split())
        j = 0
        f2.write(str(i) + ":" + str(perc) + "\n")
        i += 1
    f1.close()
    f2.close()

#writeAllowed("test1.txt","test2.txt","a")

#EX3.
def writeMandatory(file1, file2,mandatory):
    f1 = open(file1, mode = "r")
    f2 = open(file2, mode = "w")
    doc1 = f1.readlines()
    add = ""
    addList = []
    for lines in doc1:
        for words in lines.split():
            for char in words:
                if char not in mandatory:
                    add = ""
                    break
                else:
                    add = add + char
            if add != "":
                f2.write(str(lines.split().count(add)) + ": " + str(add) + "\n")
            add = ""
    f1.close()
    f2.close()
#writeMandatory("test1.txt","test2.txt","adios")

#EX4.
import random
import string
import pickle
def writeRandom(file1):
    f1 = open(file1, mode = "wb")
    magnum = []
    i = 0
    while i < 1000:
        magnum.append(random.choice(string.ascii_lowercase))
        i += 1
    pickle.dump(magnum,f1)
    f1.close()
def readRandom(file1, file2):
    f1 = open(file1, mode = "rb")
    f2 = open(file2, mode = "rb")
    L1 = pickle.load(f1)
    L2 = pickle.load(f2)
    me = 0
    i = 0
    while i < 1000:
        if L1[i] == L2[i]:
            me = me + 1
        i += 1
    print(me/len(L1))
    f1.close()
    f2.close()
writeRandom("test1.txt")
writeRandom("test2.txt")
readRandom("test1.txt", "test2.txt")
